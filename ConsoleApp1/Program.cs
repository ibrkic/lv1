﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            String text;
            String author;
            String importance;
            text=Console.ReadLine();
            author = Console.ReadLine();
            importance = Console.ReadLine();
            Note note1 = new Note(text,author,importance);
            text = Console.ReadLine();
            author = Console.ReadLine();
            importance = Console.ReadLine();
            Note note2 = new Note(text, author, importance);
            text = Console.ReadLine();
            author = Console.ReadLine();
            importance = Console.ReadLine();
            Note note3 = new Note(text, author, importance);
            ToDoList toDoList = new ToDoList();
            toDoList.Add(note1);
            toDoList.Add(note2);
            toDoList.Add(note3);
            Console.WriteLine(toDoList.PrintList());
            toDoList.DoJob("3 - High");
            Console.WriteLine(toDoList.PrintList());
        }
    }
}
