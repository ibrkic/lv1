﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ToDoList
    {
        List<Note> notes = new List<Note>();
        public ToDoList() { }
        public void Add(Note note)
        {
            notes.Add(note);
        }
        public void Remove(Note note)
        {
            notes.Remove(note);
        }

        public void DoJob (String importance)
        {
            for (int i = 0;i < notes.Count();i++)
            {
                if (notes[i].Importance==importance)
                {
                    notes.RemoveAt(i);
                }
            }
        }
        public String Get(int id)
        {
            return notes[id].ToString();
        }
        public String PrintList()
        {
            StringBuilder builder = new StringBuilder();
            foreach(Note item in notes)
            {
                builder.AppendLine("Author: " + item.Author + ", Text: " + item.Text);
            }
            return builder.ToString();
        }
    }
}
