﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class TimeStampNote:Note
    {
        DateTime dateTime;
        Note note;
        public TimeStampNote(Note note, DateTime dateTime)
        {
            this.note = note;
            this.dateTime = dateTime;
        }
        public TimeStampNote(Note note)
        {
            this.note = note;
            this.dateTime = DateTime.Now;
        }

        DateTime DateTime
        {
            get { return this.dateTime; }
            set { this.dateTime = value; }
        }

        public override string ToString()
        {
            return "Author: " + note.Author + ", Text: " + note.Text + ", Created on: " + this.dateTime;
        }

    }
}
