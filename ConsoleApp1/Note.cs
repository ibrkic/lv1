﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Note
    {
        private String text;
        private String author;
        private String importance;

        public Note(String text, String author, String importance)
        {
            this.author = author;
            this.text = text;
            this.importance = importance;
        }

        public Note(String text, String author)
        {
            this.author = author;
            this.text = text;
            this.importance = "1 - Low";
        }

        public Note()
        {
            this.author = "Default author";
            this.text = "Default text";
            this.importance = "2 - Medium";
        }


        public void SetText (String text) { this.text = text; }
        public void SetImportance (String importance) { this.importance = importance; }
        public String GetAuthor () { return this.author; }
        public String GetText() { return this.text; }
        public String Author
        {
            get { return this.author; }
        }
        public String Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public String Importance
        {
            get { return this.importance; }
            set { this.importance = value; }
        }
        public override string ToString()
        {
            return "Author: " + this.author + ", Text: " + this.text;
        }
    }
}
